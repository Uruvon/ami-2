export default class Constants {
  public static COLORS = {
    blue: '#00B0FF',
    yellow: '#FFEB3B',
    red: '#F50057',
    green: '#76FF03',
    white: '#FFF',
    lightRed: '#F77'
  };

  public static VOLUME = {
    minSteps: 1,
    maxSteps: 1024
  };
}
