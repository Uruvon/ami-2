import Constants from '../Constants';
import { SliceRenderer } from '../renderers/SliceRenderer';

export class SliceBorderHelper extends THREE.Object3D {
  private _mesh: THREE.Line;
  private _geometry: THREE.BufferGeometry;
  private _material: THREE.LineBasicMaterial;

  private _helpersSlice: SliceRenderer;
  get helpersSlice(): SliceRenderer {
    return this._helpersSlice;
  }
  set helpersSlice(helpersSlice: SliceRenderer) {
    this._helpersSlice = helpersSlice;
    this._update();
  }

  private _visible: boolean;
  get visible(): boolean {
    return this._visible;
  }
  set visible(visible: boolean) {
    this._visible = visible;
    if (this._mesh) {
      this._mesh.visible = this._visible;
    }
  }

  private _color: string;
  get color(): string {
    return this._color;
  }
  set color(color: string) {
    this._color = color;
    if (this._material) {
      this._material.color.set(this._color);
    }
  }

  constructor(helpersSlice: SliceRenderer) {
    super();

    this._helpersSlice = helpersSlice;

    this._visible = true;
    this._color = Constants.COLORS.white;
    this._material = null;
    this._geometry = null;
    this._mesh = null;

    this._create();
  }

  public _create() {
    if (!this._material) {
      this._material = new THREE.LineBasicMaterial({
        color: this._color
      });
    }

    if (
      !this._helpersSlice.geometry ||
      !this._helpersSlice.geometry.attributes.vertices
    ) {
      return;
    }

    this._geometry = new THREE.BufferGeometry();

    // set vertices positions
    const nbOfVertices = this._helpersSlice.geometry.attributes.vertices.length;
    const positions = new Float32Array((nbOfVertices + 1) * 3);
    positions.set(this._helpersSlice.geometry.attributes.position.array, 0);
    positions.set(
      this._helpersSlice.geometry.attributes.vertices[0].toArray(),
      nbOfVertices * 3
    );
    this._geometry.addAttribute(
      'position',
      new THREE.Float32BufferAttribute(positions, 3)
    );

    this._mesh = new THREE.Line(this._geometry, this._material);
    if (this._helpersSlice.aaBBSpace === 'IJK') {
      this._mesh.applyMatrix(this._helpersSlice.stack.ijk2LPS);
    }
    this._mesh.visible = this._visible;

    // and add it!
    this.add(this._mesh);
  }

  public _update() {
    // update slice
    if (this._mesh) {
      this.remove(this._mesh);
      this._mesh.geometry.dispose();
      this._mesh = null;
    }

    this._create();
  }

  public dispose() {
    (this._mesh.material as THREE.LineBasicMaterial).dispose();
    this._mesh.material = null;
    this._geometry.dispose();
    this._geometry = null;
    this._material.dispose();
    this._material = null;
  }
}
