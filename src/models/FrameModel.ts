import {BaseModel} from './BaseModel';

/**
 * Frame object.
 */
export class FrameModel extends BaseModel {
  private _sopInstanceUID: number;
  public get sopInstanceUID(): number {
    return this._sopInstanceUID;
  }
  public set sopInstanceUID(value: number) {
    this._sopInstanceUID = value;
  }

  private _url: string;
  public get url(): string {
    return this._url;
  }
  public set url(value: string) {
    this._url = value;
  }

  //private _stackID: number;

  private _invert: boolean;
  public get invert(): boolean {
    return this._invert;
  }
  public set invert(value: boolean) {
    this._invert = value;
  }

  private _frameTime: number;
  get frameTime(): number {
    return this._frameTime;
  }
  set frameTime(frameTime: number) {
    this._frameTime = frameTime;
  }

  private _rows: number;
  get rows(): number {
    return this._rows;
  }
  set rows(rows: number) {
    this._rows = rows;
  }

  private _columns: number;
  get columns(): number {
    return this._columns;
  }
  set columns(columns: number) {
    this._columns = columns;
  }

  private _dimensionIndexValues: number[];
  public get dimensionIndexValues(): number[] {
    return this._dimensionIndexValues;
  }
  public set dimensionIndexValues(value: number[]) {
    this._dimensionIndexValues = value;
  }

  private _imagePosition: number[];
  public get imagePosition(): number[] {
    return this._imagePosition;
  }
  public set imagePosition(value: number[]) {
    this._imagePosition = value;
  }

  private _imageOrientation: number[];
  public get imageOrientation(): number[] {
    return this._imageOrientation;
  }
  public set imageOrientation(value: number[]) {
    this._imageOrientation = value;
  }

  private _rightHanded: boolean;
  public get rightHanded(): boolean {
    return this._rightHanded;
  }
  public set rightHanded(value: boolean) {
    this._rightHanded = value;
  }

  private _sliceThickness: number;
  get sliceThickness(): number {
    return this._sliceThickness;
  }
  set sliceThickness(sliceThickness: number) {
    this._sliceThickness = sliceThickness;
  }

  private _spacingBetweenSlices: number;
  get spacingBetweenSlices(): number {
    return this._spacingBetweenSlices;
  }
  set spacingBetweenSlices(spacingBetweenSlices: number) {
    this._spacingBetweenSlices = spacingBetweenSlices;
  }

  private _pixelPaddingValue: number;
  public get pixelPaddingValue(): number {
    return this._pixelPaddingValue;
  }
  public set pixelPaddingValue(value: number) {
    this._pixelPaddingValue = value;
  }

  private _pixelRepresentation: number;
  public get pixelRepresentation(): number {
    return this._pixelRepresentation;
  }
  public set pixelRepresentation(value: number) {
    this._pixelRepresentation = value;
  }

  private _pixelType: number;
  public get pixelType(): number {
    return this._pixelType;
  }
  public set pixelType(value: number) {
    this._pixelType = value;
  }

  private _pixelSpacing: number;
  public get pixelSpacing(): number {
    return this._pixelSpacing;
  }
  public set pixelSpacing(value: number) {
    this._pixelSpacing = value;
  }

  private _pixelAspectRatio: any;

  private _pixelData: any;
  public get pixelData(): any {
    //window.console.log('FrameModel: pixelData ', typeof this._pixelData);
    return this._pixelData;
  }
  public set pixelData(value: any) {
    this._pixelData = value;
  }

  private _instanceNumber: number;
  public get instanceNumber(): number {
    return this._instanceNumber;
  }
  public set instanceNumber(value: number) {
    this._instanceNumber = value;
  }

  private _windowCenter: number;
  public get windowCenter(): number {
    return this._windowCenter;
  }
  public set windowCenter(value: number) {
    this._windowCenter = value;
  }

  private _windowWidth: number;
  public get windowWidth(): number {
    return this._windowWidth;
  }
  public set windowWidth(value: number) {
    this._windowWidth = value;
  }

  private _rescaleSlope: number;
  public get rescaleSlope(): number {
    return this._rescaleSlope;
  }
  public set rescaleSlope(value: number) {
    this._rescaleSlope = value;
  }

  private _rescaleIntercept: number;
  public get rescaleIntercept(): number {
    return this._rescaleIntercept;
  }
  public set rescaleIntercept(value: number) {
    this._rescaleIntercept = value;
  }

  private _bitsAllocated: number;
  public get bitsAllocated(): number {
    return this._bitsAllocated;
  }
  public set bitsAllocated(value: number) {
    this._bitsAllocated = value;
  }

  private _numberOfChannels: number;
  public get numberOfChannels(): number {
    return this._numberOfChannels;
  }
  public set numberOfChannels(value: number) {
    this._numberOfChannels = value;
  }

  private _minMax: number[];
  public get minMax(): number[] {
    return this._minMax;
  }
  public set minMax(value: number[]) {
    this._minMax = value;
  }

  private _distance: number;
  public get distance(): number {
    return this._distance;
  }
  public set distance(value: number) {
    this._distance = value;
  }

  private _index: number;
  public get index(): number {
    return this._index;
  }
  public set index(value: number) {
    this._index = value;
  }

  private _referencedSegmentNumber: number;
  public get referencedSegmentNumber(): number {
    return this._referencedSegmentNumber;
  }
  public set referencedSegmentNumber(value: number) {
    this._referencedSegmentNumber = value;
  }

  constructor() {
    super();

    this._sopInstanceUID = null;
    this._url = null;
    //this._stackID = -1;
    this._invert = false;
    this._frameTime = null;
    this._rows = 0;
    this._columns = 0;
    this._dimensionIndexValues = [];
    this._imagePosition = null;
    this._imageOrientation = null;
    this._rightHanded = true;
    this._sliceThickness = 1;
    this._spacingBetweenSlices = null;
    this._pixelPaddingValue = null;
    this._pixelRepresentation = 0;
    this._pixelType = 0;
    this._pixelSpacing = null;
    this._pixelAspectRatio = null;
    this._pixelData = null;

    this._instanceNumber = null;
    this._windowCenter = null;
    this._windowWidth = null;
    this._rescaleSlope = null;
    this._rescaleIntercept = null;

    this._bitsAllocated = 8;
    this._numberOfChannels = 1;

    this._minMax = null;
    this._distance = null;

    this._index = -1;

    this._referencedSegmentNumber = -1;
  }

  /**
   * Merge current frame with provided frame.
   *
   * Frames can be merged (i.e. are identical) if following are equals:
   *  - dimensionIndexValues
   *  - imageOrientation
   *  - imagePosition
   *  - instanceNumber
   *  - sopInstanceUID
   */
  public merge(frame: FrameModel): boolean {
    if (
      this._compareArrays(
        this._dimensionIndexValues,
        frame.dimensionIndexValues
      ) &&
      this._compareArrays(this._imageOrientation, frame.imageOrientation) &&
      this._compareArrays(this._imagePosition, frame.imagePosition) &&
      this._instanceNumber === frame.instanceNumber &&
      this._sopInstanceUID === frame.sopInstanceUID
    ) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Generate X, y and Z cosines from image orientation
   * Returns default orientation if _imageOrientation was invalid.
   */
  public cosines(): THREE.Vector3[] {
    const cosines = [
      new THREE.Vector3(1, 0, 0),
      new THREE.Vector3(0, 1, 0),
      new THREE.Vector3(0, 0, 1)
    ];

    if (this._imageOrientation && this._imageOrientation.length === 6) {
      const xCos = new THREE.Vector3(
        this._imageOrientation[0],
        this._imageOrientation[1],
        this._imageOrientation[2]
      );
      const yCos = new THREE.Vector3(
        this._imageOrientation[3],
        this._imageOrientation[4],
        this._imageOrientation[5]
      );

      if (xCos.length() > 0 && yCos.length() > 0) {
        cosines[0] = xCos;
        cosines[1] = yCos;
        cosines[2] = new THREE.Vector3(0, 0, 0)
          .crossVectors(cosines[0], cosines[1])
          .normalize();
      }
    } else {
      window.console.log('No valid image orientation for frame');
      window.console.log(this);
      window.console.log('Returning default orientation.');
    }

    if (!this._rightHanded) {
      cosines[2].negate();
    }

    return cosines;
  }

  /**
   * Get x/y spacing of a frame.
   */
  public spacingXY(): number[] {
    const spacingXY = [1.0, 1.0];

    if (this.pixelSpacing) {
      spacingXY[0] = this.pixelSpacing[0];

      spacingXY[1] = this.pixelSpacing[1];
    } else if (this._pixelAspectRatio) {
      spacingXY[0] = 1.0;
      spacingXY[1] =
        (1.0 * this._pixelAspectRatio[1]) / this._pixelAspectRatio[0];
    }

    return spacingXY;
  }

  /**
   * Get data value
   */
  public getPixelData(column: number, row: number): any {
    if (column >= 0 && column < this._columns && row >= 0 && row < this._rows) {
      return this._pixelData[column + this._columns * row];
    } else {
      return null;
    }
  }
  /**
   * Set data value
   */
  public setPixelData(column: number, row: number, value: any) {
    this._pixelData[column + this._columns * row] = value;
  }

  /**
   * Get frame preview as data:URL
   */
  public getImageDataUrl(): string {
    const canvas = document.createElement('canvas');
    canvas.width = this._columns;
    canvas.height = this._rows;

    const context = canvas.getContext('2d');

    const imageData = context.createImageData(canvas.width, canvas.height);

    imageData.data.set(this._frameToCanvas());
    context.putImageData(imageData, 0, 0);

    return canvas.toDataURL();
  }

  /**
   * Convert frame.pixelData to canvas.context.imageData.data
   */
  public _frameToCanvas(): Uint8Array {
    const dimension = this._columns * this._rows;

    // TODO: Hacky re-arrange, assume it;'s working
    const params = {
      invert: this._invert,
      min: this._minMax[0],
      max: this._minMax[1],
      range: 0,
      padding: this._pixelPaddingValue
    };
    const data = new Uint8Array(dimension * 4);

    if (params.padding !== null) {
      // recalculation of min ignoring pixelPaddingValue
      params.min = this._minMax[1];
      for (
        let index = 0, numPixels = this._pixelData.length;
        index < numPixels;
        index++
      ) {
        if (this._pixelData[index] !== params.padding) {
          params.min = Math.min(params.min, this._pixelData[index]);
        }
      }
    }

    if (this._windowWidth && this._windowCenter !== null) {
      // applying windowCenter and windowWidth
      const intercept = this._rescaleIntercept || 0;

      const slope = this._rescaleSlope || 1;

      params.min = Math.max(
        (this._windowCenter - this._windowWidth / 2 - intercept) / slope,
        params.min
      );
      params.max = Math.min(
        (this._windowCenter + this._windowWidth / 2 - intercept) / slope,
        this._minMax[1]
      );
    } else {
      params.max = this._minMax[1];
    }

    params.range = params.max - params.min || 255; // if max is 0 convert it to: 255 - black, 1 - white

    if (this._numberOfChannels === 1) {
      for (let i = 0; i < dimension; i++) {
        const normalized = this._pixelTo8Bit(this._pixelData[i], params);
        data[4 * i] = normalized;
        data[4 * i + 1] = normalized;
        data[4 * i + 2] = normalized;
        data[4 * i + 3] = 255; // alpha channel (fully opaque)
      }
    } else if (this._numberOfChannels === 3) {
      for (let i = 0; i < dimension; i++) {
        data[4 * i] = this._pixelTo8Bit(this._pixelData[3 * i], params);
        data[4 * i + 1] = this._pixelTo8Bit(this._pixelData[3 * i + 1], params);
        data[4 * i + 2] = this._pixelTo8Bit(this._pixelData[3 * i + 2], params);
        data[4 * i + 3] = 255; // alpha channel (fully opaque)
      }
    }

    return data;
  }

  /**
   * Convert pixel value to 8 bit (canvas.context.imageData.data: maximum 8 bit per each of RGBA value)
   *
   * @param {Number} value  Pixel value
   * @param {Object} params {invert, min, mix, padding, range}
   *
   * @return {Number}
   */
  public _pixelTo8Bit(
    value: number,
    params: {
      invert: boolean;
      min: number;
      max: number;
      padding: number;
      range: number;
    }
  ) {
    // values equal to pixelPaddingValue are outside of the image and should be ignored
    let packedValue = value <= params.min || value === params.padding ? 0 : 255;

    if (value > params.min && value < params.max) {
      packedValue = Math.round(((value - params.min) * 255) / params.range);
    }

    return Number.NaN === packedValue
      ? 0
      : params.invert
      ? 255 - packedValue
      : packedValue;
  }

  /**
   * Compare 2 arrays.
   *
   * 2 null arrays return true.
   * Do no perform strict type checking.
   */
  public _compareArrays(reference: any[], target: any[]): boolean {
    // could both be null
    if (reference === target) {
      return true;
    }

    // if not null....
    if (reference && target && reference.join() === target.join()) {
      return true;
    }

    return false;
  }
}
