import binaryString from 'math-float32-to-binary-string';
import {CoreColors} from '../core/CoreColours';
import {CoreUtils} from '../core/CoreUtils';
import { AMISegment } from '../interfaces/AMISegment';
import {BaseModel} from './BaseModel';
import { FrameModel } from './FrameModel';

export class StackModel extends BaseModel {
  private _prepared: boolean;
  public get prepared() {
    return this._prepared;
  }

  private _packed: boolean;
  public get packed() {
    return this._packed;
  }

  private _dimensionsIJK: THREE.Vector3;
  public get dimensionsIJK() {
    return this._dimensionsIJK.clone();
  }
  public get halfDimensionsIJK() {
    return this._dimensionsIJK.clone().divideScalar(2);
  }

  private _ijk2LPS: THREE.Matrix4;
  public get ijk2LPS() {
    return this._ijk2LPS;
  }

  private _textureSize: number;
  public get textureSize() {
    return this._textureSize;
  }

  // tslint:disable-next-line: no-any
  private _rawData: any[];
  // tslint:disable-next-line: no-any
  public get rawData(): any[] {
    //window.console.log('StackModel: rawData ', typeof this._rawData);
    return this._rawData;
  }

  // tslint:disable-next-line: no-any
  private _textureType: any;
  // tslint:disable-next-line: no-any
  public get textureType(): any {
    //window.console.log('StackModel: textureType ', typeof this._textureType);
    return this._textureType;
  }
  // tslint:disable-next-line: no-any
  public set textureType(value: any) {
    this._textureType = value;
  }

  private _spacing: THREE.Vector3;
  public get spacing() {
    return this._spacing;
  }

  private _minMax: number[];
  public get minMax(): number[] {
    return this._minMax;
  }

  private _stackID: number;
  public get stackID(): number {
    return this._stackID;
  }

  private _frames: FrameModel[];
  public get frames(): FrameModel[] {
    return this._frames;
  }

  //private _frameSegment: FrameModel[];
  private _numberOfFrames: number;
  private _rows: number;
  private _columns: number;

  private _numberOfChannels: number;
  public get numberOfChannels(): number {
    return this._numberOfChannels;
  }
  public set numberOfChannels(value: number) {
    this._numberOfChannels = value;
  }

  private _bitsAllocated: number;
  public get bitsAllocated(): number {
    return this._bitsAllocated;
  }

  private _pixelType: number;
  public get pixelType(): number {
    return this._pixelType;
  }
  public set pixelType(value: number) {
    this._pixelType = value;
  }

  private _pixelRepresentation: number;
  public get pixelRepresentation(): number {
    return this._pixelRepresentation;
  }
  public set pixelRepresentation(value: number) {
    this._pixelRepresentation = value;
  }

  private _textureUnits: number;

  private _windowCenter: number;
  public get windowCenter(): number {
    return this._windowCenter;
  }

  private _windowWidth: number;
  public get windowWidth(): number {
    return this._windowWidth;
  }

  private _rescaleSlope: number;
  public get rescaleSlope(): number {
    return this._rescaleSlope;
  }

  private _rescaleIntercept: number;
  public get rescaleIntercept(): number {
    return this._rescaleIntercept;
  }

  private _regMatrix: THREE.Matrix4;

  private _lps2IJK: THREE.Matrix4;
  public get lps2IJK(): THREE.Matrix4 {
    return this._lps2IJK;
  }

  private _aabb2LPS: THREE.Matrix4;

  private _lps2AABB: THREE.Matrix4;
  public get lps2AABB(): THREE.Matrix4 {
    return this._lps2AABB;
  }

  private _spacingBetweenSlices: number;
  public get spacingBetweenSlices(): number {
    return this._spacingBetweenSlices;
  }
  public set spacingBetweenSlices(value: number) {
    this._spacingBetweenSlices = value;
  }

  private _origin: THREE.Vector3;
  
  private _rightHanded: boolean;
  public get rightHanded(): boolean {
    return this._rightHanded;
  }
  public set rightHanded(value: boolean) {
    this._rightHanded = value;
  }

  private _xCosine: THREE.Vector3;
  public get xCosine(): THREE.Vector3 {
    return this._xCosine;
  }

  private _yCosine: THREE.Vector3;
  public get yCosine(): THREE.Vector3 {
    return this._yCosine;
  }

  private _zCosine: THREE.Vector3;
  public get zCosine(): THREE.Vector3 {
    return this._zCosine;
  }

  private _packedPerPixel: number;
  public get packedPerPixel(): number {
    return this._packedPerPixel;
  }

  private _modality: string;
  public get modality(): string {
    return this._modality;
  }
  public set modality(value: string) {
    this._modality = value;
  }

  private _segmentationType: any;
  public get segmentationType(): any {
    return this._segmentationType;
  }
  public set segmentationType(value: any) {
    this._segmentationType = value;
  }

  private _segmentationSegments: AMISegment[];
  public get segmentationSegments(): AMISegment[] {
    return this._segmentationSegments;
  }
  public set segmentationSegments(value: AMISegment[]) {
    this._segmentationSegments = value;
  }

  private _segmentationDefaultColor: number[];
  private _segmentationLUT: any[];
  private _segmentationLUTO: any[];

  private _invert: boolean;
  public get invert(): boolean {
    return this._invert;
  }
  public set invert(value: boolean) {
    this._invert = value;
  }

  constructor() {
    super();

    this._stackID = -1;

    this._frames = [];
    this._numberOfFrames = 0;

    this._rows = 0;
    this._columns = 0;
    this._numberOfChannels = 1;
    this._bitsAllocated = 8;
    this._pixelType = 0;
    this._pixelRepresentation = 0;

    this._textureSize = 4096;
    this._textureUnits = 7;

    this._rawData = [];

    this._windowCenter = 0;
    this._windowWidth = 0;

    this._rescaleSlope = 1;
    this._rescaleIntercept = 0;

    this._minMax = [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY];

    // TRANSFORMATION MATRICES
    this._regMatrix = new THREE.Matrix4();

    this._ijk2LPS = null;
    this._lps2IJK = null;

    this._aabb2LPS = null;
    this._lps2AABB = null;

    //
    // IJK dimensions
    this._dimensionsIJK = null;
    this._spacing = new THREE.Vector3(1, 1, 1);
    this._spacingBetweenSlices = 0;
    this._origin = null;
    //this._rightHanded = true;
    this._xCosine = new THREE.Vector3(1, 0, 0);
    this._yCosine = new THREE.Vector3(0, 1, 0);
    this._zCosine = new THREE.Vector3(0, 0, 1);

    // convenience vars
    this._prepared = false;
    this._packed = false;
    this._packedPerPixel = 1;

    //
    this._modality = 'Modality not set';

    // SEGMENTATION STUFF
    this._segmentationType = null;
    this._segmentationSegments = [];
    this._segmentationDefaultColor = [63, 174, 128];
    //this._frameSegment = [];
    this._segmentationLUT = [];
    this._segmentationLUTO = [];

    // photometricInterpretation Monochrome1 VS Monochrome2
    this._invert = false;
  }

  /**
   * Prepare segmentation stack.
   * A segmentation stack can hold x frames that are at the same location
   * but segmentation specific information:
   * - Frame X contains voxels for segmentation A.
   * - Frame Y contains voxels for segmenttation B.
   * - Frame X and Y are at the same location.
   *
   * We currently merge overlaping frames into 1.
   */
  public prepareSegmentation() {
    // store frame and do special pre-processing
    //this._frameSegment = this._frames;
    const mergedFrames = [];

    // order frames
    this.computeCosines();
    this._frames.map(this._computedistanceArrayMap.bind(null, this._zCosine));
    this._frames.sort(this._sortdistanceArraySort);

    // merge frames
    let prevIndex = -1;
    for (const datum of this._frames) {
      if (datum) {
        if (
          !mergedFrames[prevIndex] ||
          mergedFrames[prevIndex]._distance !== datum.distance
        ) {
          mergedFrames.push(datum);
          prevIndex++;

          // Scale frame
          // by default each frame contains binary data about a segmentation.
          // we scale it by the referenceSegmentNumber in order to have a
          // segmentation specific voxel value rather than 0 or 1.
          // That allows us to merge frames later on.
          // If we merge frames without scaling, then we can not differenciate
          // voxels from segmentation A or B as the value is 0 or 1 in both cases.
          for (
            let k = 0;
            k <
            mergedFrames[prevIndex]._rows * mergedFrames[prevIndex]._columns;
            k++
          ) {
            mergedFrames[prevIndex]._pixelData[k] *=
            datum.referencedSegmentNumber;
          }
        } else {
          // frame already exsists at this location.
          // merge data from this segmentation into existing frame
          for (
            let k = 0;
            k <
            mergedFrames[prevIndex]._rows * mergedFrames[prevIndex]._columns;
            k++
          ) {
            mergedFrames[prevIndex]._pixelData[k] +=
            datum.pixelData[k] * datum.referencedSegmentNumber;
          }
        }

        mergedFrames[prevIndex].minMax = CoreUtils.minMax(
          mergedFrames[prevIndex]._pixelData
        );
      }
    }

    // get information about segments
    const dict = {};
    let max = 0;
    for (const segment of this._segmentationSegments) {
      if (segment) {
        max = Math.max(max, parseInt(segment.segmentNumber, 10));

        const color = segment.recommendedDisplayCIELab;
        if (color === null) {
          dict[segment.segmentNumber] = this._segmentationDefaultColor;
        } else {
          dict[segment.segmentNumber] = CoreColors.cielab2RGB(...color);
        }
      }
    }

    // generate LUTs
    for (let i = 0; i <= max; i++) {
      const index = i / max;
      const opacity = i ? 1 : 0;
      let rgb = [0, 0, 0];
      if (dict.hasOwnProperty(i.toString())) {
        rgb = dict[i.toString()];
      }

      rgb[0] /= 255;
      rgb[1] /= 255;
      rgb[2] /= 255;

      this._segmentationLUT.push([index, ...rgb]);
      this._segmentationLUTO.push([index, opacity]);
    }

    this._frames = mergedFrames;
  }

  public prepare() {
    // if segmentation, merge some frames...
    if (this._modality === 'SEG') {
      this.prepareSegmentation();
    }

    this.computeNumberOfFrames();

    // pass parameters from frame to stack
    this._rows = this._frames[0].rows;
    this._columns = this._frames[0].columns;
    this._dimensionsIJK = new THREE.Vector3(
      this._columns,
      this._rows,
      this._numberOfFrames
    );
    this._spacingBetweenSlices = this._frames[0].spacingBetweenSlices;

    // compute direction cosines
    this.computeCosines();

    // order the frames
    if (this._numberOfFrames > 1) {
      this.orderFrames();
    }

    // compute/guess spacing
    this.computeSpacing();
    // set extra vars if nulls
    // do it now because before we would think image position/orientation
    // are defined and we would use it to compute spacing.
    if (!this._frames[0].imagePosition) {
      this._frames[0].imagePosition = [0, 0, 0];
    }
    if (!this._frames[0].imageOrientation) {
      this._frames[0].imageOrientation = [1, 0, 0, 0, 1, 0];
    }

    this._origin = this._arrayToVector3(this._frames[0].imagePosition, 0);

    // compute transforms
    this.computeIJK2LPS();

    this.computeLPS2AABB();
    // this.packEchos();

    const middleFrameIndex = Math.floor(this._frames.length / 2);
    const middleFrame = this._frames[middleFrameIndex];

    this._rescaleSlope = middleFrame.rescaleSlope || 1;
    this._rescaleIntercept = middleFrame.rescaleIntercept || 0;

    // rescale/slope min max
    this.computeMinMaxIntensities();
    this._minMax[0] = CoreUtils.rescaleSlopeIntercept(
      this._minMax[0],
      this._rescaleSlope,
      this._rescaleIntercept
    );
    this._minMax[1] = CoreUtils.rescaleSlopeIntercept(
      this._minMax[1],
      this._rescaleSlope,
      this._rescaleIntercept
    );

    this._windowWidth =
      middleFrame.windowWidth || this._minMax[1] - this._minMax[0];

    this._windowCenter =
      middleFrame.windowCenter || this._minMax[0] + this._windowWidth / 2;

    this._bitsAllocated = middleFrame.bitsAllocated;
    this._prepared = true;
  }

  public packEchos() {
    // 4 echo times...
    const echos = 4;
    const packedEcho = [];
    for (let i = 0; i < this._frames.length; i += echos) {
      const frame = this._frames[i];
      for (let k = 0; k < this._rows * this._columns; k++) {
        for (let j = 1; j < echos; j++) {
          frame.pixelData[k] += this._frames[i + j].pixelData[k];
        }
        frame.pixelData[k] /= echos;
      }
      packedEcho.push(frame);
    }
    this._frames = packedEcho;
    this._numberOfFrames = this._frames.length;
    this._dimensionsIJK = new THREE.Vector3(
      this._columns,
      this._rows,
      this._numberOfFrames
    );
  }

  public computeNumberOfFrames() {
    // we need at least 1 frame
    if (this._frames && this._frames.length > 0) {
      this._numberOfFrames = this._frames.length;
    } else {
      window.console.error("the _frame doesn't contain anything");
      window.console.error(this._frames);
      return false;
    }
  }

  // frame.cosines - returns array [x, y, z]
  public computeCosines() {
    if (this._frames && this._frames[0]) {
      const cosines = this._frames[0].cosines();
      this._xCosine = cosines[0];
      this._yCosine = cosines[1];
      this._zCosine = cosines[2];
    }
  }

  public orderFrames() {
    // order the frames based on theirs dimension indices
    // first index is the most important.
    // 1,1,1,1 will be first
    // 1,1,2,1 will be next
    // 1,1,2,3 will be next
    // 1,1,3,1 will be next
    if (this._frames[0].dimensionIndexValues) {
      this._frames.sort(this._orderFrameOnDimensionIndicesArraySort);

      // else order with image position and orientation
    } else if (
      this._frames[0].imagePosition &&
      this._frames[0].imageOrientation &&
      this._frames[1] &&
      this._frames[1].imagePosition &&
      this._frames[1].imageOrientation &&
      this._frames[0].imagePosition.join() !==
        this._frames[1].imagePosition.join()
    ) {
      // compute and sort by distance in this series
      this._frames.map(this._computedistanceArrayMap.bind(null, this._zCosine));
      this._frames.sort(this._sortdistanceArraySort);
    } else if (
      this._frames[0].instanceNumber !== null &&
      this._frames[1] &&
      this._frames[1].instanceNumber !== null &&
      this._frames[0].instanceNumber !== this._frames[1].instanceNumber
    ) {
      this._frames.sort(this._sortInstanceNumberArraySort);
    } else if (
      this._frames[0].sopInstanceUID &&
      this._frames[1] &&
      this._frames[1].sopInstanceUID &&
      this._frames[0].sopInstanceUID !== this._frames[1].sopInstanceUID
    ) {
      this._frames.sort(this._sortSopInstanceUIDArraySort);
    } else if (!this._frames[0].imagePosition) {
      // cancel warning if you have set null imagePosition on purpose (?)
    } else {
      window.console.warn('do not know how to order the frames...');
    }
  }

  public computeSpacing() {
    this.xySpacing();
    this.zSpacing();
  }

  /**
   * Compute stack z spacing
   */
  public zSpacing() {
    if (this._numberOfFrames > 1) {
      if (this._frames[0].pixelSpacing && this._frames[0].pixelSpacing[2]) {
        this._spacing.z = this._frames[0].pixelSpacing[2];
      } else {
        // compute and sort by distance in this series
        this._frames.map(
          this._computedistanceArrayMap.bind(null, this._zCosine)
        );

        // if distances are different, re-sort array
        if (this._frames[1].distance !== this._frames[0].distance) {
          this._frames.sort(this._sortdistanceArraySort);
          this._spacing.z = this._frames[1].distance - this._frames[0].distance;
        } else if (this._spacingBetweenSlices) {
          this._spacing.z = this._spacingBetweenSlices;
        } else if (this._frames[0].sliceThickness) {
          this._spacing.z = this._frames[0].sliceThickness;
        }
      }
    }

    // Spacing
    // can not be 0 if not matrix can not be inverted.
    if (this._spacing.z === 0) {
      this._spacing.z = 1;
    }
  }

  /**
   *  FRAME CAN DO IT
   */
  public xySpacing() {
    if (this._frames && this._frames[0]) {
      const spacingXY = this._frames[0].spacingXY();
      this._spacing.x = spacingXY[0];
      this._spacing.y = spacingXY[1];
    }
  }

  /**
   * Find min and max intensities among all frames.
   */
  public computeMinMaxIntensities() {
    // we ignore values if NaNs
    // https://github.com/FNNDSC/ami/issues/185
    for (const datum of this._frames) {
      if (datum) {
        // get min/max
        const min = datum.minMax[0];
        if (!Number.isNaN(min)) {
          this._minMax[0] = Math.min(this._minMax[0], min);
        }

        const max = datum.minMax[1];
        if (!Number.isNaN(max)) {
          this._minMax[1] = Math.max(this._minMax[1], max);
        }
      }
    }
  }

  /**
   * Compute IJK to LPS and invert transforms
   */
  public computeIJK2LPS() {
    // ijk to lps
    this._ijk2LPS = CoreUtils.ijk2LPS(
      this._xCosine,
      this._yCosine,
      this._zCosine,
      this._spacing,
      this._origin,
      this._regMatrix
    );

    // lps 2 ijk
    this._lps2IJK = new THREE.Matrix4();
    this._lps2IJK.getInverse(this._ijk2LPS);
  }

  /**
   * Compute LPS to AABB and invert transforms
   */
  public computeLPS2AABB() {
    this._aabb2LPS = CoreUtils.aabb2LPS(
      this._xCosine,
      this._yCosine,
      this._zCosine,
      this._origin
    );

    this._lps2AABB = new THREE.Matrix4();
    this._lps2AABB.getInverse(this._aabb2LPS);
  }

  /**
   * Merge stacks
   *
   * @param {*} stack
   *
   * @return {*}
   */
  public merge(stack: StackModel) {
    // also make sure x/y/z cosines are a match!
    if (
      this._stackID === stack.stackID &&
      this._numberOfFrames === 1 &&
      stack._numberOfFrames === 1 &&
      this._frames[0].columns === stack.frames[0].columns &&
      this._frames[0].rows === stack.frames[0].rows &&
      this._xCosine.equals(stack.xCosine) &&
      this._yCosine.equals(stack.yCosine) &&
      this._zCosine.equals(stack.zCosine)
    ) {
      return this.mergeModels(this._frames, stack.frames);
    } else {
      return false;
    }
  }

  /**
   * Pack current stack pixel data into 8 bits array buffers
   */
  public pack() {
    // Get total number of voxels
    const nbVoxels =
      this._dimensionsIJK.x * this._dimensionsIJK.y * this._dimensionsIJK.z;

    // Packing style
    if (
      (this._bitsAllocated === 8 && this._numberOfChannels === 1) ||
      this._bitsAllocated === 1
    ) {
      this._packedPerPixel = 4;
    }

    if (this._bitsAllocated === 16 && this._numberOfChannels === 1) {
      this._packedPerPixel = 2;
    }

    // Loop through all the textures we need
    const textureDimension = this._textureSize * this._textureSize;
    let requiredTextures = Math.ceil(
      nbVoxels / (textureDimension * this._packedPerPixel)
    );
    let voxelIndexStart = 0;
    let voxelIndexStop = this._packedPerPixel * textureDimension;
    if (voxelIndexStop > nbVoxels) {
      voxelIndexStop = nbVoxels;
    }

    if (this._textureUnits < requiredTextures) {
      console.warn(
        'Insufficient number of supported textures. Some frames will not be packed.'
      );
      requiredTextures = this._textureUnits;
    }

    for (let ii = 0; ii < requiredTextures; ii++) {
      const packed = this._packTo8Bits(
        this._numberOfChannels,
        this._frames,
        this._textureSize,
        voxelIndexStart,
        voxelIndexStop
      );
      this._textureType = packed.textureType;
      this._rawData.push(packed.data);

      voxelIndexStart += this._packedPerPixel * textureDimension;
      voxelIndexStop += this._packedPerPixel * textureDimension;
      if (voxelIndexStop > nbVoxels) {
        voxelIndexStop = nbVoxels;
      }
    }

    this._packed = true;
  }

  /**
   * Pack frame data to 32 bits texture
   * @param {*} channels
   * @param {*} frame
   * @param {*} textureSize
   * @param {*} startVoxel
   * @param {*} stopVoxel
   */
  public _packTo8Bits(
    channels: number,
    frames: FrameModel[],
    // tslint:disable-next-line: no-any
    textureSize: any,
    // tslint:disable-next-line: no-any
    startVoxel: any,
    // tslint:disable-next-line: no-any
    stopVoxel: any
  ) {
    // window.console.log('StackModel textureSixe: ', typeof textureSize);
    // window.console.log('StackModel startVoxel: ', typeof startVoxel);
    // window.console.log('StackModel stopVoxel: ', typeof stopVoxel);
    const packed = {
      textureType: null,
      data: null
    };

    const bitsAllocated = frames[0].bitsAllocated;
    const pixelType = frames[0].pixelType;

    // transform signed to unsigned for convenience
    let offset = 0;
    if (this._minMax[0] < 0) {
      offset -= this._minMax[0];
    }

    let packIndex = 0;
    let frameIndex = 0;
    let inFrameIndex = 0;
    // frame should return it!
    const frameDimension = frames[0].rows * frames[0].columns;

    if ((bitsAllocated === 8 && channels === 1) || bitsAllocated === 1) {
      const data = new Uint8Array(textureSize * textureSize * 4);
      let coordinate = 0;
      let channelOffset = 0;
      for (let i = startVoxel; i < stopVoxel; i++) {
        // tslint:disable-next-line: no-bitwise
        frameIndex = ~~(i / frameDimension);
        inFrameIndex = i % frameDimension;

        const raw = frames[frameIndex].pixelData[inFrameIndex] + offset;
        if (!Number.NaN === raw) {
          data[4 * coordinate + channelOffset] = raw;
        }

        packIndex++;
        coordinate = Math.floor(packIndex / 4);
        channelOffset = packIndex % 4;
      }
      packed.textureType = THREE.RGBAFormat;
      packed.data = data;
    } else if (bitsAllocated === 16 && channels === 1) {
      const data = new Uint8Array(textureSize * textureSize * 4);
      let coordinate = 0;
      let channelOffset = 0;

      for (let i = startVoxel; i < stopVoxel; i++) {
        // tslint:disable-next-line: no-bitwise
        frameIndex = ~~(i / frameDimension);
        inFrameIndex = i % frameDimension;

        const raw = frames[frameIndex].pixelData[inFrameIndex] + offset;
        if (!Number.NaN === raw) {
          /* tslint:disable:no-bitwise */
          data[4 * coordinate + 2 * channelOffset] = raw & 0x00ff;
          data[4 * coordinate + 2 * channelOffset + 1] = (raw >>> 8) & 0x00ff;
          /* tslint:enable:no-bitwise */
        }

        packIndex++;
        coordinate = Math.floor(packIndex / 2);
        channelOffset = packIndex % 2;
      }

      packed.textureType = THREE.RGBAFormat;
      packed.data = data;
    } else if (bitsAllocated === 32 && channels === 1 && pixelType === 0) {
      const data = new Uint8Array(textureSize * textureSize * 4);
      for (let i = startVoxel; i < stopVoxel; i++) {
        // tslint:disable-next-line: no-bitwise
        frameIndex = ~~(i / frameDimension);
        inFrameIndex = i % frameDimension;

        const raw = frames[frameIndex].pixelData[inFrameIndex] + offset;
        if (!Number.NaN === raw) {
          /* tslint:disable:no-bitwise */
          data[4 * packIndex] = raw & 0x000000ff;
          data[4 * packIndex + 1] = (raw >>> 8) & 0x000000ff;
          data[4 * packIndex + 2] = (raw >>> 16) & 0x000000ff;
          data[4 * packIndex + 3] = (raw >>> 24) & 0x000000ff;
          /* tslint:enable:no-bitwise */
        }

        packIndex++;
      }
      packed.textureType = THREE.RGBAFormat;
      packed.data = data;
    } else if (bitsAllocated === 32 && channels === 1 && pixelType === 1) {
      const data = new Uint8Array(textureSize * textureSize * 4);

      for (let i = startVoxel; i < stopVoxel; i++) {
        // tslint:disable-next-line: no-bitwise
        frameIndex = ~~(i / frameDimension);
        inFrameIndex = i % frameDimension;

        const raw = frames[frameIndex].pixelData[inFrameIndex] + offset;
        if (!Number.NaN === raw) {
          const bitString = binaryString(raw);
          const bitStringArray = bitString.match(/.{1,8}/g);

          data[4 * packIndex] = parseInt(bitStringArray[0], 2);
          data[4 * packIndex + 1] = parseInt(bitStringArray[1], 2);
          data[4 * packIndex + 2] = parseInt(bitStringArray[2], 2);
          data[4 * packIndex + 3] = parseInt(bitStringArray[3], 2);
        }

        packIndex++;
      }

      packed.textureType = THREE.RGBAFormat;
      packed.data = data;
    } else if (bitsAllocated === 8 && channels === 3) {
      const data = new Uint8Array(textureSize * textureSize * 3);

      for (let i = startVoxel; i < stopVoxel; i++) {
        // tslint:disable-next-line: no-bitwise
        frameIndex = ~~(i / frameDimension);
        inFrameIndex = i % frameDimension;

        data[3 * packIndex] = frames[frameIndex].pixelData[3 * inFrameIndex];
        data[3 * packIndex + 1] =
          frames[frameIndex].pixelData[3 * inFrameIndex + 1];
        data[3 * packIndex + 2] =
          frames[frameIndex].pixelData[3 * inFrameIndex + 2];
        packIndex++;
      }

      packed.textureType = THREE.RGBFormat;
      packed.data = data;
    }

    return packed;
  }

  /**
   * Get the stack world center
   *
   * @return {*}
   */
  public worldCenter() {
    const center = this.halfDimensionsIJK
      .clone()
      .addScalar(-0.5)
      .applyMatrix4(this._ijk2LPS);
    return center;
  }

  /**
   * Get the stack world bounding box
   * @return {*}
   */
  public worldBoundingBox() {
    let bbox = [
      Number.POSITIVE_INFINITY,
      Number.NEGATIVE_INFINITY,
      Number.POSITIVE_INFINITY,
      Number.NEGATIVE_INFINITY,
      Number.POSITIVE_INFINITY,
      Number.NEGATIVE_INFINITY
    ];

    const dims = this._dimensionsIJK;

    for (let i = 0; i <= dims.x; i += dims.x) {
      for (let j = 0; j <= dims.y; j += dims.y) {
        for (let k = 0; k <= dims.z; k += dims.z) {
          const world = new THREE.Vector3(i, j, k).applyMatrix4(this._ijk2LPS);
          bbox = [
            Math.min(bbox[0], world.x),
            Math.max(bbox[1], world.x), // x min/max
            Math.min(bbox[2], world.y),
            Math.max(bbox[3], world.y),
            Math.min(bbox[4], world.z),
            Math.max(bbox[5], world.z)
          ];
        }
      }
    }

    return bbox;
  }

  /**
   * Get AABB size in LPS space.
   *
   * @return {*}
   */
  public AABBox() {
    const world0 = new THREE.Vector3()
      .addScalar(-0.5)
      .applyMatrix4(this._ijk2LPS)
      .applyMatrix4(this._lps2AABB);

    const world7 = this._dimensionsIJK
      .clone()
      .addScalar(-0.5)
      .applyMatrix4(this._ijk2LPS)
      .applyMatrix4(this._lps2AABB);

    const minBBox = new THREE.Vector3(
      Math.abs(world0.x - world7.x),
      Math.abs(world0.y - world7.y),
      Math.abs(world0.z - world7.z)
    );

    return minBBox;
  }

  /**
   * Get AABB center in LPS space
   */
  public centerAABBox() {
    const centerBBox = this.worldCenter();
    centerBBox.applyMatrix4(this._lps2AABB);
    return centerBBox;
  }

  public static indexInDimensions(index: THREE.Vector3, dimensions: THREE.Vector3) {
    if (
      index.x >= 0 &&
      index.y >= 0 &&
      index.z >= 0 &&
      index.x < dimensions.x &&
      index.y < dimensions.y &&
      index.z < dimensions.z
    ) {
      return true;
    }

    return false;
  }

  public _arrayToVector3(array: number[], index: number) {
    return new THREE.Vector3(array[index], array[index + 1], array[index + 2]);
  }

  public _orderFrameOnDimensionIndicesArraySort(a: FrameModel, b: FrameModel) {
    if (
      'dimensionIndexValues' in a &&
      Object.prototype.toString.call(a.dimensionIndexValues) ===
        '[object Array]' &&
      'dimensionIndexValues' in b &&
      Object.prototype.toString.call(b.dimensionIndexValues) ===
        '[object Array]'
    ) {
      for (let i = 0; i < a.dimensionIndexValues.length; i++) {
        if (
          parseInt(a.dimensionIndexValues[i].toString(), 10) >
          parseInt(b.dimensionIndexValues[i].toString(), 10)
        ) {
          return 1;
        }
        if (
          parseInt(a.dimensionIndexValues[i].toString(), 10) <
          parseInt(b.dimensionIndexValues[i].toString(), 10)
        ) {
          return -1;
        }
      }
    } else {
      window.console.warn(
        "One of the frames doesn't have a dimensionIndexValues array."
      );
      window.console.warn(a);
      window.console.warn(b);
    }

    return 0;
  }

  public _computedistanceArrayMap(normal: THREE.Vector3, frame: FrameModel) {
    if (frame.imagePosition) {
      frame.distance =
        frame.imagePosition[0] * normal.x +
        frame.imagePosition[1] * normal.y +
        frame.imagePosition[2] * normal.z;
    }
    return frame;
  }

  public _sortdistanceArraySort(a: FrameModel, b: FrameModel) {
    return a.distance - b.distance;
  }
  public _sortInstanceNumberArraySort(a: FrameModel, b: FrameModel) {
    return a.instanceNumber - b.instanceNumber;
  }
  public _sortSopInstanceUIDArraySort(a: FrameModel, b: FrameModel) {
    return a.sopInstanceUID - b.sopInstanceUID;
  }
}
