/**
 * Base object.
 */

export class BaseModel {
  protected _id: number;

  constructor() {
    this._id = -1;
  }

  /**
   * Merge 2 arrays of models.
   * Merge the target array into the reference array.
   */
  public mergeModels(
    referenceArray: BaseModel[],
    targetArray: BaseModel[]
  ): boolean {
    if (
      !(
        this._validateModelArray(referenceArray) &&
        this._validateModelArray(targetArray)
      )
    ) {
      window.console.log('invalid inputs provided.');
      return false;
    }

    for (let i = 0, targetLength = targetArray.length; i < targetLength; i++) {
      // test targetArray against existing targetArray
      for (let j = 0, refLength = referenceArray.length; j < refLength; j++) {
        if (referenceArray[j].merge(targetArray[i])) {
          // merged successfully
          break;
        } else if (j === referenceArray.length - 1) {
          // last merge was not successful
          // this is a new targetArray
          referenceArray.push(targetArray[i]);
        }
      }
    }

    return true;
  }

  /**
   * Merge model against current model.
   */
  public merge(model: BaseModel) {
    // make sure model is valid
    if (!this.validate(model)) {
      return false;
    }

    // they can be merged if they match
    if (this._id === model._id) {
      return true;
    }
    return false;
  }

  /**
   * Validate a model.
   */
  public validate(model: BaseModel): boolean {
    if (!(model && model !== null && typeof model.merge === 'function')) {
      return false;
    }

    return true;
  }

  /**
   * Validate array of models.
   */
  public _validateModelArray(modelArray: BaseModel[]): boolean {
    if (!(modelArray !== null && Array === modelArray.constructor)) {
      window.console.log('invalid model array provided.');
      return false;
    }

    for (const model of modelArray) {
      if (model) {
        if (
          !(
            model &&
            model !== null &&
            typeof model.validate === 'function' &&
            model.validate(model)
          )
        ) {
          return false;
        }
      }
    }

    return true;
  }
}
