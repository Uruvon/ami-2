import { AMIaabb } from '../interfaces/AMIaabb';
import { AMIBbox } from '../interfaces/AMIbbox';
import { AMIPlane } from '../interfaces/AMIPlane';
import { AMIRay } from '../interfaces/AMIRay';
import {CoreUtils} from './CoreUtils';

/**
 * Compute/test intersection between different objects.
 *
 * @module core/intersections
 */

export class CoreIntersections {
  /**
   * Compute intersection between oriented bounding box and a plane.
   *
   * Returns intersection in plane's space.
   *
   * Should return at least 3 intersections. If not, the plane and the box do not
   * intersect.
   */
  public static aabbPlane(aabb: AMIaabb, plane: AMIPlane) {
    //
    // obb = { halfDimensions, orientation, center, toAABB }
    // plane = { position, direction }
    //
    //
    // LOGIC:
    //
    // Test intersection of each edge of the Oriented Bounding Box with the Plane
    //
    // ALL EDGES
    //
    //      .+-------+
    //    .' |     .'|
    //   +---+---+'  |
    //   |   |   |   |
    //   |  ,+---+---+
    //   |.'     | .'
    //   +-------+'
    //
    // SPACE ORIENTATION
    //
    //       +
    //     j |
    //       |
    //       |   i
    //   k  ,+-------+
    //    .'
    //   +
    //
    //
    // 1- Move Plane position and orientation in IJK space
    // 2- Test Edges/ IJK Plane intersections
    // 3- Return intersection Edge/ IJK Plane if it touches the Oriented BBox

    const intersections = [];

    // invert space matrix
    const fromAABB = new THREE.Matrix4();
    fromAABB.getInverse(aabb.toAABB);

    const t1 = plane.direction.clone().applyMatrix4(aabb.toAABB);
    const t0 = new THREE.Vector3(0, 0, 0).applyMatrix4(aabb.toAABB);

    const planeAABB = this.posdir(
      plane.position.clone().applyMatrix4(aabb.toAABB),
      new THREE.Vector3(t1.x - t0.x, t1.y - t0.y, t1.z - t0.z).normalize()
    );

    const bbox = CoreUtils.bbox(aabb.center, aabb.halfDimensions);

    // 12 edges (i.e. ray)/plane intersection tests
    // RAYS STARTING FROM THE FIRST CORNER (0, 0, 0)
    //
    //       +
    //       |
    //       |
    //       |
    //      ,+---+---+
    //    .'
    //   +

    const orientationX = new THREE.Vector3(1, 0, 0);

    const orientationY = new THREE.Vector3(0, 1, 0);

    const orientationZ = new THREE.Vector3(0, 0, 1);

    const ray = this.posdir(
      new THREE.Vector3(
        aabb.center.x - aabb.halfDimensions.x,
        aabb.center.y - aabb.halfDimensions.y,
        aabb.center.z - aabb.halfDimensions.z
      ),
      orientationX.clone()
    );

    if (bbox) {
      this.rayPlaneInBBox(ray, planeAABB, bbox, intersections);

      ray.direction = orientationY.clone();
      this.rayPlaneInBBox(ray, planeAABB, bbox, intersections);

      ray.direction = orientationZ.clone();
      this.rayPlaneInBBox(ray, planeAABB, bbox, intersections);
    }

    // RAYS STARTING FROM THE LAST CORNER
    //
    //               +
    //             .'
    //   +-------+'
    //           |
    //           |
    //           |
    //           +
    //

    const ray2 = this.posdir(
      new THREE.Vector3(
        aabb.center.x + aabb.halfDimensions.x,
        aabb.center.y + aabb.halfDimensions.y,
        aabb.center.z + aabb.halfDimensions.z
      ),
      orientationX.clone()
    );

    if (bbox) {
      this.rayPlaneInBBox(ray2, planeAABB, bbox, intersections);

      ray2.direction = orientationY.clone();
      this.rayPlaneInBBox(ray2, planeAABB, bbox, intersections);

      ray2.direction = orientationZ.clone();
      this.rayPlaneInBBox(ray2, planeAABB, bbox, intersections);
    }

    // RAYS STARTING FROM THE SECOND CORNER
    //
    //               +
    //               |
    //               |
    //               |
    //               +
    //             .'
    //           +'

    const ray3 = this.posdir(
      new THREE.Vector3(
        aabb.center.x + aabb.halfDimensions.x,
        aabb.center.y - aabb.halfDimensions.y,
        aabb.center.z - aabb.halfDimensions.z
      ),
      orientationY.clone()
    );

    if (bbox) {
      this.rayPlaneInBBox(ray3, planeAABB, bbox, intersections);

      ray3.direction = orientationZ.clone();
      this.rayPlaneInBBox(ray3, planeAABB, bbox, intersections);
    }

    // RAYS STARTING FROM THE THIRD CORNER
    //
    //      .+-------+
    //    .'
    //   +
    //
    //
    //
    //

    const ray4 = this.posdir(
      new THREE.Vector3(
        aabb.center.x - aabb.halfDimensions.x,
        aabb.center.y + aabb.halfDimensions.y,
        aabb.center.z - aabb.halfDimensions.z
      ),
      orientationX.clone()
    );

    if (bbox) {
      this.rayPlaneInBBox(ray4, planeAABB, bbox, intersections);

      ray4.direction = orientationZ.clone();
      this.rayPlaneInBBox(ray4, planeAABB, bbox, intersections);
    }

    // RAYS STARTING FROM THE FOURTH CORNER
    //
    //
    //
    //   +
    //   |
    //   |
    //   |
    //   +-------+

    const ray5 = this.posdir(
      new THREE.Vector3(
        aabb.center.x - aabb.halfDimensions.x,
        aabb.center.y - aabb.halfDimensions.y,
        aabb.center.z + aabb.halfDimensions.z
      ),
      orientationX.clone()
    );

    if (bbox) {
      this.rayPlaneInBBox(ray5, planeAABB, bbox, intersections);

      ray5.direction = orientationY.clone();
      this.rayPlaneInBBox(ray5, planeAABB, bbox, intersections);
    }

    // @todo make sure objects are unique...

    // back to original space
    intersections.map(element => {
      return element.applyMatrix4(fromAABB);
    });

    return intersections;
  }

  /**
   * Compute intersection between a ray and a plane.
   *
   * @memberOf this
   * @public
   *
   * @param {Object} ray - Ray representation.
   * @param {Vector3} ray.position - position of normal which describes the ray.
   * @param {Vector3} ray.direction - Direction of normal which describes the ray.
   * @param {Object} plane - Plane representation
   * @param {Vector3} plane.position - position of normal which describes the plane.
   * @param {Vector3} plane.direction - Direction of normal which describes the plane.
   *
   * @returns {Vector3|null} Intersection between ray and plane or null.
   */
  public static rayPlane(ray: AMIRay, plane: AMIPlane) {
    // ray: {position, direction}
    // plane: {position, direction}

    if (ray.direction.dot(plane.direction) !== 0) {
      //
      // not parallel, move forward
      //
      // LOGIC:
      //
      // Ray equation: P = P0 + tV
      // P = <Px, Py, Pz>
      // P0 = <ray.position.x, ray.position.y, ray.position.z>
      // V = <ray.direction.x, ray.direction.y, ray.direction.z>
      //
      // Therefore:
      // Px = ray.position.x + t*ray.direction.x
      // Py = ray.position.y + t*ray.direction.y
      // Pz = ray.position.z + t*ray.direction.z
      //
      //
      //
      // Plane equation: ax + by + cz + d = 0
      // a = plane.direction.x
      // b = plane.direction.y
      // c = plane.direction.z
      // d = -( plane.direction.x*plane.position.x +
      //        plane.direction.y*plane.position.y +
      //        plane.direction.z*plane.position.z )
      //
      //
      // 1- in the plane equation, we replace x, y and z by Px, Py and Pz
      // 2- find t
      // 3- replace t in Px, Py and Pz to get the coordinate of the intersection
      //
      const t =
        (plane.direction.x * (plane.position.x - ray.position.x) +
          plane.direction.y * (plane.position.y - ray.position.y) +
          plane.direction.z * (plane.position.z - ray.position.z)) /
        (plane.direction.x * ray.direction.x +
          plane.direction.y * ray.direction.y +
          plane.direction.z * ray.direction.z);

      const intersection = new THREE.Vector3(
        ray.position.x + t * ray.direction.x,
        ray.position.y + t * ray.direction.y,
        ray.position.z + t * ray.direction.z
      );

      return intersection;
    }

    return null;
  }

  /**
   * Compute intersection between a ray and a box
   * @param {Object} ray
   * @param {Object} box
   * @return {Array}
   */
  public static rayBox(ray: AMIRay, box: AMIaabb) {
    // should also do the space transforms here
    // ray: {position, direction}
    // box: {halfDimensions, center}

    const intersections = [];

    const bbox = CoreUtils.bbox(box.center, box.halfDimensions);

    if (bbox) {
      // X min
      let plane = this.posdir(
        new THREE.Vector3(bbox.min.x, box.center.y, box.center.z),
        new THREE.Vector3(-1, 0, 0)
      );
      this.rayPlaneInBBox(ray, plane, bbox, intersections);

      // X max
      plane = this.posdir(
        new THREE.Vector3(bbox.max.x, box.center.y, box.center.z),
        new THREE.Vector3(1, 0, 0)
      );
      this.rayPlaneInBBox(ray, plane, bbox, intersections);

      // Y min
      plane = this.posdir(
        new THREE.Vector3(box.center.x, bbox.min.y, box.center.z),
        new THREE.Vector3(0, -1, 0)
      );
      this.rayPlaneInBBox(ray, plane, bbox, intersections);

      // Y max
      plane = this.posdir(
        new THREE.Vector3(box.center.x, bbox.max.y, box.center.z),
        new THREE.Vector3(0, 1, 0)
      );
      this.rayPlaneInBBox(ray, plane, bbox, intersections);

      // Z min
      plane = this.posdir(
        new THREE.Vector3(box.center.x, box.center.y, bbox.min.z),
        new THREE.Vector3(0, 0, -1)
      );
      this.rayPlaneInBBox(ray, plane, bbox, intersections);

      // Z max
      plane = this.posdir(
        new THREE.Vector3(box.center.x, box.center.y, bbox.max.z),
        new THREE.Vector3(0, 0, 1)
      );
      this.rayPlaneInBBox(ray, plane, bbox, intersections);
    }

    return intersections;
  }

  /**
   * Intersection between ray and a plane that are in a box.
   */
  public static rayPlaneInBBox(
    ray: AMIRay,
    planeAABB: AMIPlane,
    bbox: AMIBbox,
    intersections: THREE.Vector3[]
  ) {
    const intersection = this.rayPlane(ray, planeAABB);
    // window.console.log(intersection);
    if (intersection && this.inBBox(intersection, bbox)) {
      // TODO: DOne because array.find doesn't work nicely with THREE.Vector3 in typescript
      // tslint:disable-next-line: no-any
      if (!(intersections as any).find(this.findIntersection(intersection))) {
        intersections.push(intersection);
      }
    }
  }

  /**
   * Find intersection in array
   */
  public static findIntersection(myintersection: THREE.Vector3) {
    return function found(element: THREE.Vector3, _index: number, _array: []) {
      if (
        myintersection.x === element.x &&
        myintersection.y === element.y &&
        myintersection.z === element.z
      ) {
        return true;
      }

      return false;
    };
  }

  /**
   * Is point in box.
   */
  public static inBBox(point: THREE.Vector3, bbox: AMIBbox) {
    //
    const epsilon = 0.0001;
    if (
      point &&
      point.x >= bbox.min.x - epsilon &&
      point.y >= bbox.min.y - epsilon &&
      point.z >= bbox.min.z - epsilon &&
      point.x <= bbox.max.x + epsilon &&
      point.y <= bbox.max.y + epsilon &&
      point.z <= bbox.max.z + epsilon
    ) {
      return true;
    }
    return false;
  }

  public static posdir(position: THREE.Vector3, direction: THREE.Vector3) {
    return { position, direction };
  }
}
