import { AMIBbox } from '../interfaces/AMIbbox';
import { StackModel } from '../models/StackModel';
/**
 * General purpose functions.
 *
 * @module core/utils
 */
export class CoreUtils {
  public static timeout(ms: number): Promise<{}> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**
   * Generate a bouding box object.
   */
  public static bbox(
    center: THREE.Vector3,
    halfDimensions: THREE.Vector3
  ): AMIBbox | false {
    // make sure half dimensions are >= 0
    if (
      !(halfDimensions.x >= 0 && halfDimensions.y >= 0 && halfDimensions.z >= 0)
    ) {
      window.console.warn('CoreUtil: halfDimensions must be >= 0.');
      window.console.warn(halfDimensions);
      return false;
    }

    // min/max bound
    const min = center.clone().sub(halfDimensions);
    const max = center.clone().add(halfDimensions);

    return {
      min,
      max
    };
  }

  /**
   * Find min/max values in an array
   */
  public static minMax(data: number[] = []) {
    const minMax = [65535, -32768];
    const numPixels = data.length;

    for (let index = 0; index < numPixels; index++) {
      const spv = data[index];
      minMax[0] = Math.min(minMax[0], spv);
      minMax[1] = Math.max(minMax[1], spv);
    }

    return minMax;
  }

  /**
   * Parse url and find out the extension of the exam file.
   */
  public static parseUrl(url: string) {
    const parsedUrl = new URL(url, 'http://fix.me');
    const data = {
      filename: parsedUrl.searchParams.get('filename'),
      extension: '',
      pathname: parsedUrl.pathname,
      query: parsedUrl.search
    };

    // get file name
    if (!data.filename) {
      data.filename = data.pathname.split('/').pop();
    }

    // find extension
    const splittedName = data.filename.split('.');

    data.extension = splittedName.length > 1 ? splittedName.pop() : 'dicom';

    const skipExt = [
      'asp',
      'aspx',
      'go',
      'gs',
      'hs',
      'jsp',
      'js',
      'php',
      'pl',
      'py',
      'rb',
      'htm',
      'html'
    ];

    if (
      !(data.extension === null) ||
      skipExt.indexOf(data.extension) !== -1 ||
      // TODO: Fix weird typing issue for strings
      // tslint:disable-next-line: no-any
      (data.query &&
        (data.query as any).includes('contentType=application%2Fdicom'))
    ) {
      data.extension = 'dicom';
    }

    return data;
  }

  /**
   * Compute IJK to LPS tranform.
   *  http://nipy.org/nibabel/dicom/dicom_orientation.html
   */
  public static ijk2LPS(
    xCos: THREE.Vector3,
    yCos: THREE.Vector3,
    zCos: THREE.Vector3,
    spacing: THREE.Vector3,
    origin: THREE.Vector3,
    registrationMatrix: THREE.Matrix4 = new THREE.Matrix4()
  ) {
    const ijk2LPS = new THREE.Matrix4();
    ijk2LPS.set(
      xCos.x * spacing.y,
      yCos.x * spacing.x,
      zCos.x * spacing.z,
      origin.x,
      xCos.y * spacing.y,
      yCos.y * spacing.x,
      zCos.y * spacing.z,
      origin.y,
      xCos.z * spacing.y,
      yCos.z * spacing.x,
      zCos.z * spacing.z,
      origin.z,
      0,
      0,
      0,
      1
    );
    ijk2LPS.premultiply(registrationMatrix);

    return ijk2LPS;
  }

  /**
   * Compute AABB to LPS transform.
   * AABB: Axe Aligned Bounding Box.
   */
  public static aabb2LPS(
    xCos: THREE.Vector3,
    yCos: THREE.Vector3,
    zCos: THREE.Vector3,
    origin: THREE.Vector3
  ) {
    const aabb2LPS = new THREE.Matrix4();
    aabb2LPS.set(
      xCos.x,
      yCos.x,
      zCos.x,
      origin.x,
      xCos.y,
      yCos.y,
      zCos.y,
      origin.y,
      xCos.z,
      yCos.z,
      zCos.z,
      origin.z,
      0,
      0,
      0,
      1
    );

    return aabb2LPS;
  }

  /**
   * Transform coordinates from world coordinate to data
   */
  public static worldToData(lps2IJK: THREE.Matrix4, worldCoordinates: THREE.Vector3) {
    const dataCoordinate = new THREE.Vector3()
      .copy(worldCoordinates)
      .applyMatrix4(lps2IJK);

    // same rounding in the shaders
    dataCoordinate.addScalar(0.5).floor();

    return dataCoordinate;
  }

  public static value(stack: StackModel, coordinate: THREE.Vector3) {
    window.console.warn('value is deprecated, please use getPixelData instead');
    this.getPixelData(stack, coordinate);
  }

  /**
   * Get voxel value
   */
  public static getPixelData(stack: StackModel, coordinate: THREE.Vector3) {
    if (coordinate.z >= 0 && coordinate.z < stack.frames.length) {
      return stack.frames[coordinate.z].getPixelData(
        coordinate.x,
        coordinate.y
      );
    } else {
      return null;
    }
  }

  /**
   * Set voxel value
   */
  public static setPixelData(
    stack: StackModel,
    coordinate: THREE.Vector3,
    value: number
  ) {
    if (coordinate.z >= 0 && coordinate.z < stack.frames.length) {
      stack.frames[coordinate.z].setPixelData(
        coordinate.x,
        coordinate.y,
        value
      );
    } else {
      return null;
    }
  }

  /**
   * Apply slope/intercept to a value
   */
  public static rescaleSlopeIntercept(
    value: number,
    slope: number,
    intercept: number
  ) {
    return value * slope + intercept;
  }

  /**
   *
   * Convenience function to extract center of mass from list of points.
   */
  public static centerOfMass(points: THREE.Vector3[]): THREE.Vector3 {
    const centerOfMass = new THREE.Vector3(0, 0, 0);
    points.forEach(element => {
      centerOfMass.x += element.x;
      centerOfMass.y += element.y;
      centerOfMass.z += element.z;
    });

    centerOfMass.divideScalar(points.length);

    return centerOfMass;
  }

  /**
   *
   * Order 3D planar points around a refence point.
   *
   * @private
   *
   * @param {Array<Vector3>} points - Set of planar 3D points to be ordered.
   * @param {Vector3} direction - Direction of the plane in which points and reference are sitting.
   *
   * @returns {Array<Object>} Set of object representing the ordered points.
   */
  public static orderIntersections(
    points: THREE.Vector3[],
    direction: THREE.Vector3
  ): any[] {
    const reference = this.centerOfMass(points);
    // direction from first point to reference
    const referenceDirection = new THREE.Vector3(
      points[0].x - reference.x,
      points[0].y - reference.y,
      points[0].z - reference.z
    ).normalize();

    const base = new THREE.Vector3(0, 0, 0)
      .crossVectors(referenceDirection, direction)
      .normalize();

    const orderedpoints = [];

    // other lines // if inter, return location + angle
    points.forEach(element => {
      const point = {
        position: new THREE.Vector3(element.x, element.y, element.z),
        direction: new THREE.Vector3(
          element.x - reference.x,
          element.y - reference.y,
          element.z - reference.z
        ).normalize(),
        xy: {
          x: 0,
          y: 0
        },
        angle: 0
      };

      const x = referenceDirection.dot(point.direction);
      const y = base.dot(point.direction);
      point.xy = { x, y };

      const theta = Math.atan2(y, x) * (180 / Math.PI);
      point.angle = theta;

      orderedpoints.push(point);
    });

    orderedpoints.sort((a, b) => {
      return a.angle - b.angle;
    });

    const noDups = [orderedpoints[0]];
    const epsilon = 0.0001;
    for (let i = 1; i < orderedpoints.length; i++) {
      if (
        Math.abs(orderedpoints[i - 1].angle - orderedpoints[i].angle) > epsilon
      ) {
        noDups.push(orderedpoints[i]);
      }
    }

    return noDups;
  }

  /**
   * Get min, max, mean and sd of voxel values behind the mesh
   */
  public static getRoI(mesh: THREE.Mesh, camera: THREE.Camera, stack: StackModel): {} {
    mesh.geometry.computeBoundingBox();

    const bbox = new THREE.Box3().setFromObject(mesh);
    const min = bbox.min.clone().project(camera);
    const max = bbox.max.clone().project(camera);
    const offsetWidth = (camera as any).controls.domElement.offsetWidth;
    const offsetHeight = (camera as any).controls.domElement.offsetHeight;
    const rayCaster = new THREE.Raycaster();
    const values = [];

    min.x = Math.round(((min.x + 1) * offsetWidth) / 2);
    min.y = Math.round(((-min.y + 1) * offsetHeight) / 2);
    max.x = Math.round(((max.x + 1) * offsetWidth) / 2);
    max.y = Math.round(((-max.y + 1) * offsetHeight) / 2);
    [min.x, max.x] = [Math.min(min.x, max.x), Math.max(min.x, max.x)];
    [min.y, max.y] = [Math.min(min.y, max.y), Math.max(min.y, max.y)];

    let intersect = [];
    let value = null;

    for (let x = min.x; x <= max.x; x++) {
      for (let y = min.y; y <= max.y; y++) {
        rayCaster.setFromCamera(
          {
            x: (x / offsetWidth) * 2 - 1,
            y: -(y / offsetHeight) * 2 + 1
          },
          camera
        );
        intersect = rayCaster.intersectObject(mesh);

        if (intersect.length === 0) {
          continue;
        }

        value = CoreUtils.getPixelData(
          stack,
          CoreUtils.worldToData(stack.lps2IJK, intersect[0].point)
        );

        // the image isn't RGB and coordinates are inside it
        if (value !== null && stack.numberOfChannels === 1) {
          values.push(
            CoreUtils.rescaleSlopeIntercept(
              value,
              stack.rescaleSlope,
              stack.rescaleIntercept
            )
          );
        }
      }
    }

    if (values.length === 0) {
      return null;
    }

    const avg = values.reduce((sum, val) => sum + val) / values.length;

    return {
      min: values.reduce((prev, val) => (prev < val ? prev : val)),
      max: values.reduce((prev, val) => (prev > val ? prev : val)),
      mean: avg,
      sd: Math.sqrt(
        values.reduce((sum, val) => sum + Math.pow(val - avg, 2), 0) /
          values.length
      )
    };
  }

  /**
   * Calculate shape area (sum of triangle polygons area).
   * May be inaccurate or completely wrong for some shapes.
   */
  public static getGeometryArea(geometry: THREE.Geometry): number {
    if (geometry.faces.length < 1) {
      return 0.0;
    }

    let area = 0.0;
    const vertices = geometry.vertices;

    geometry.faces.forEach(elem => {
      area += new THREE.Triangle(
        vertices[elem.a],
        vertices[elem.b],
        vertices[elem.c]
      ).getArea();
    });

    return area;
  }
}
