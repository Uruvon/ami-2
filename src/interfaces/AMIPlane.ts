export interface AMIPlane {
  position: THREE.Vector3;
  direction: THREE.Vector3;
}
