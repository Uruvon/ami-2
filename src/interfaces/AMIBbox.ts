export interface AMIBbox {
  min: THREE.Vector3;
  max: THREE.Vector3;
}
