export interface AMIRay {
  position: THREE.Vector3;
  direction: THREE.Vector3;
}
