// Axis Aligned Bounding Box
export interface AMIaabb {
  halfDimensions: THREE.Vector3;
  center: THREE.Vector3;
  toAABB: THREE.Matrix4;
}
