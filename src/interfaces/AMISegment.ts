export interface AMISegment {
  recommendedDisplayCIELab: number[];
  segmentationCodeDesignator: string;
  segmentationCodeValue: string;
  segmentationCodeMeaning: string;
  segmentNumber: any;
  segmentLabel: any;
  segmentAlgorithmType: any;
}
