export class SliceUtils {
  /**
   * Compute slice index depending on orientation.
   *
   * @param {number} orientation - The orientation of the stackHelper.
   * @param {Vector3} indices - Indices in each direction.
   *
   * @returns {number} Slice index according to current orientation.
   *
   * @private
   */
  public static prepareSliceIndex(
    orientation: number,
    indices: THREE.Vector3
  ): number {
    let index = 0;
    switch (orientation) {
      case 0:
        index = Math.floor(indices.z);
        break;
      case 1:
        index = Math.floor(indices.x);
        break;
      case 2:
        index = Math.floor(indices.y);
        break;
      default:
        // do nothing!
        break;
    }
    return index;
  }

  /**
   * Compute slice position depending on orientation.
   * Sets index in proper location of reference position.
   *
   * @param {number} orientation - The orientation of the stackHelper.
   * @param {Vector3} rPosition - Reference position.
   * @param {number} index - Current index.
   *
   * @returns {number} Slice index according to current orientation.
   *
   * @private
   */
  public static prepareSlicePosition(
    orientation: number,
    rPosition: THREE.Vector3,
    index: number
  ): THREE.Vector3 {
    let position = new THREE.Vector3(0, 0, 0);
    switch (orientation) {
      case 0:
        position = new THREE.Vector3(
          Math.floor(rPosition.x),
          Math.floor(rPosition.y),
          index
        );
        break;
      case 1:
        position = new THREE.Vector3(
          index,
          Math.floor(rPosition.y),
          Math.floor(rPosition.z)
        );
        break;
      case 2:
        position = new THREE.Vector3(
          Math.floor(rPosition.x),
          index,
          Math.floor(rPosition.z)
        );
        break;
      default:
        // do nothing!
        break;
    }
    return position;
  }

  /**
   * Compute slice direction depending on orientation.
   *
   * @param {number} orientation - Slice orientation.
   *
   * @returns {Vector3} Slice direction
   *
   * @private
   */
  public static prepareDirection(orientation: number): THREE.Vector3 {
    let direction = new THREE.Vector3(0, 0, 1);
    switch (orientation) {
      case 0:
        direction = new THREE.Vector3(0, 0, 1);
        break;
      case 1:
        direction = new THREE.Vector3(1, 0, 0);
        break;
      case 2:
        direction = new THREE.Vector3(0, 1, 0);
        break;
      default:
        // do nothing!
        break;
    }

    return direction;
  }
}
