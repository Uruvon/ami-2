import glslify from 'glslify';

export class MaterialUtils {
  public static processSource(source: string): string {
    const output = source
      .split(/"/)[1]
      .replace(/(\\r)/gm, '')
      .replace(/(\\n)/gm, '\n ');
    const res = glslify(output);

    return res;
  }
}
