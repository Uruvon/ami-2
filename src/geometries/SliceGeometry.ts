import { CoreIntersections } from '../core/CoreIntersections';
import { CoreUtils } from '../core/CoreUtils';
import { AMIPlane } from '../interfaces/AMIPlane';

/**
 *
 * It is typically used for creating an irregular 3D planar shape given a box and the cut-plane.
 *
 * Demo: {@link https://fnndsc.github.io/vjs#geometry_slice}
 *
 * @module geometries/slice
 *
 * @param {Vector3} halfDimensions - Half-dimensions of the box to be sliced.
 * @param {Vector3} center - Center of the box to be sliced.
 * @param {Vector3<Vector3>} orientation - Orientation of the box to be sliced. (might not be necessary..?)
 * @param {Vector3} position - Position of the cutting plane.
 * @param {Vector3} direction - Cross direction of the cutting plane.
 *
 * @example
 * // Define box to be sliced
 * let halfDimensions = new THREE.Vector(123, 45, 67);
 * let center = new THREE.Vector3(0, 0, 0);
 * let orientation = new THREE.Vector3(
 *   new THREE.Vector3(1, 0, 0),
 *   new THREE.Vector3(0, 1, 0),
 *   new THREE.Vector3(0, 0, 1)
 * );
 *
 * // Define slice plane
 * let position = center.clone();
 * let direction = new THREE.Vector3(-0.2, 0.5, 0.3);
 *
 * // Create the slice geometry & materials
 * let sliceGeometry = new VJS.geometries.slice(halfDimensions, center, orientation, position, direction);
 * let sliceMaterial = new THREE.MeshBasicMaterial({
 *   'side': DoubleSide,
 *   'color': 0xFF5722
 * });
 *
 *  // Create mesh and add it to the scene
 *  let slice = new Mesh(sliceGeometry, sliceMaterial);
 *  scene.add(slice);
 */
export class SliceGeometry extends THREE.ShapeBufferGeometry {
  public type;
  public plane: AMIPlane;
  public aabb: {};

  constructor(
    halfDimensions: THREE.Vector3,
    center: THREE.Vector3,
    position: THREE.Vector3,
    direction: THREE.Vector3,
    toAABB: THREE.Matrix4 = new THREE.Matrix4()
  ) {
    //
    // prepare data for the shape!
    //
    const aabb = {
      halfDimensions,
      center,
      toAABB
    };

    const plane = {
      position,
      direction
    };

    // BOOM!
    const intersections = CoreIntersections.aabbPlane(aabb, plane);

    // can not exist before calling the constructor
    if (intersections && intersections.length < 3) {
      window.console.error(
        'ERROR: Less than 3 intersections between AABB and Plane.'
      );
      window.console.error('AABB');
      window.console.error(aabb);
      window.console.error('Plane');
      window.console.error(plane);
      window.console.error('exiting...');
      const err = new Error(
        'geometries.slice has less than 3 intersections, can not create a valid geometry.'
      );
      throw err;
    }

    const points = CoreUtils.orderIntersections(intersections, direction);

    // create the shape
    const shape = new THREE.Shape();
    // move to first point!
    shape.moveTo(points[0].xy.x, points[0].xy.y);

    // loop through all points!
    const positions = new Float32Array(points.length * 3);
    positions.set(points[0].position.toArray(), 0);

    for (let i = 1; i < points.length; i++) {
      // project each on plane!
      positions.set(points[i].position.toArray(), i * 3);

      shape.lineTo(points[i].xy.x, points[i].xy.y);
    }

    // close the shape!
    shape.lineTo(points[0].xy.x, points[0].xy.y);

    //
    // Generate Slice Buffer Geometry from Shape Buffer Geomtry
    // because it does triangulation for us!
    super(shape);
    this.type = 'SliceBufferGeometry';
    this.plane = plane;
    this.aabb = aabb;

    // update real position of each vertex! (not in 2d)
    this.addAttribute(
      'position',
      new THREE.Float32BufferAttribute(positions, 3)
    );
    this.computeVertexNormals();
    // this.attributes.position = points; // legacy code to compute normals in the SliceHelper
  }
}
