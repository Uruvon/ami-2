import Constants from '../Constants';
import { SliceBorderHelper } from '../helpers/SliceBorderHelper';
import { StackModel } from '../models/StackModel';
import { SliceUtils } from '../utils/SliceUtils';
import { SliceRenderer } from './SliceRenderer';

export class SlicesRenderer extends THREE.Object3D {
  private _border: SliceBorderHelper;
  public get border(): SliceBorderHelper {
    return this._border;
  }

  private _orientationSpacing: number;
  public get orientationSpacing(): number {
    return this._orientationSpacing;
  }

  private _orientationMaxIndex: number;
  public get orientationMaxIndex(): number {
    return this._orientationMaxIndex;
  }

  private _outOfBounds: boolean;
  public get outOfBounds(): boolean {
    return this._outOfBounds;
  }

  private _stack: StackModel;
  public get stack(): StackModel {
    return this._stack;
  }

  private _boundingBox: THREE.BoxHelper;
  public get boundingBox(): THREE.BoxHelper {
    return this._boundingBox;
  }

  private _color: THREE.Color;
  public get color(): THREE.Color {
    return this._color;
  }
  public set color(value: THREE.Color) {
    this._color = value;
  }

  private _sliceIndex: number;
  public get sliceIndex(): number {
    return this._sliceIndex;
  }
  set index(index: number) {
    this._sliceIndex = index;

    // update the slice
    this._slice.sliceIndex = index;

    this._slice.plane.position = SliceUtils.prepareSlicePosition(
      this._sliceOrientation,
      this._stack.halfDimensionsIJK,
      this._sliceIndex
    );

    // also update the border
    this._border.helpersSlice = this._slice;

    // update ourOfBounds flag
    this._isIndexOutOfBounds();
  }

  private _slice: SliceRenderer;
  public get slice() {
    return this._slice;
  }

  private _sliceOrientation: number;
  public get sliceOrientation() {
    return this._sliceOrientation;
  }
  public set sliceOrientation(value: number) {
    this._sliceOrientation = value;
    this._computeOrientationMaxIndex();
    this._computeOrientationSpacing();

    this._slice.spacing = Math.abs(this._orientationSpacing);
    this._slice.thickness = this._slice.spacing;

    this._slice.plane.direction = SliceUtils.prepareDirection(
      this._sliceOrientation
    );

    // also update the border
    this._border.helpersSlice = this._slice;
  }

  private _meshStack: THREE.Mesh;
  // private _canvasWidth: number;
  // private _canvasHeight: number;
  // private _borderColor: string;

  constructor(
    stack: StackModel,
    color: THREE.Color = new THREE.Color(Constants.COLORS.red)
  ) {
    super();

    this._stack = stack;
    this._color = color;

    this._boundingBox = null;
    this._slice = null;
    this._border = null;
    this._sliceOrientation = 0;
    this._sliceIndex = 0;
    this._outOfBounds = false;
    this._orientationMaxIndex = 0;
    this._orientationSpacing = 0;
    // this._canvasWidth = 0;
    // this._canvasHeight = 0;
    // this._borderColor = null;

    this._create();
  }

  private _create() {
    if (this._stack) {
      this._prepareStack();
      this._createBoundingBox();
      this._prepareSlice();
      this._prepareBorder();
    } else {
      // TODO: Add Logging function here
      window.console.error('No stack to be prepared in StackHelper');
    }
  }

  /**
   * Setup border helper given slice helper and add border helper
   * to stack helper.
   *
   * @private
   */
  private _prepareBorder() {
    this._border = new SliceBorderHelper(this._slice);
    this.add(this._border);
  }

  private _prepareStack() {
    // make sure there is something, if not throw an error
    // compute image to workd transform, order frames, etc.
    if (!this._stack.prepared) {
      this._stack.prepare();
    }
    // pack data into 8 bits rgba texture for the shader
    // this one can be slow...
    if (!this._stack.packed) {
      this._stack.pack();
    }
  }

  private _createBoundingBox() {
    // Convenience vars
    const dimensions = this._stack.dimensionsIJK;
    const halfDimensions = this._stack.halfDimensionsIJK;
    const offset = new THREE.Vector3(-0.5, -0.5, -0.5);

    // Geometry
    const geometry = new THREE.BoxGeometry(
      dimensions.x,
      dimensions.y,
      dimensions.z
    );
    geometry.applyMatrix(
      new THREE.Matrix4().makeTranslation(
        halfDimensions.x + offset.x,
        halfDimensions.y + offset.y,
        halfDimensions.z + offset.z
      )
    );

    // const material = new MeshBasicMaterial({
    //   wireframe: true,
    //   color: this._color
    // });

    const mesh = new THREE.Mesh(geometry, null);
    mesh.applyMatrix(this._stack.ijk2LPS);
    mesh.visible = this.visible;
    this._meshStack = mesh;

    this._boundingBox = new THREE.BoxHelper(this._meshStack, this._color);
    this.add(this._boundingBox);
  }

  /**
   * Setup slice helper given prepared stack helper and add slice helper
   * to stack helper.
   *
   * @private
   */
  private _prepareSlice() {
    const halfDimensionsIJK = this._stack.halfDimensionsIJK;

    // compute initial index given orientation
    this._sliceIndex = SliceUtils.prepareSliceIndex(
      this._sliceOrientation,
      halfDimensionsIJK
    );

    // compute initial position given orientation and index
    const position = SliceUtils.prepareSlicePosition(
      this._sliceOrientation,
      halfDimensionsIJK,
      this._sliceIndex
    );

    // compute initial direction orientation
    const direction = SliceUtils.prepareDirection(this._sliceOrientation);

    this._slice = new SliceRenderer(
      this._stack,
      this._sliceIndex,
      position,
      direction
    );

    this.add(this._slice);
  }

  private _computeOrientationMaxIndex() {
    const dimensionsIJK = this._stack.dimensionsIJK;
    this._orientationMaxIndex = 0;
    switch (this._sliceOrientation) {
      case 0:
        this._orientationMaxIndex = dimensionsIJK.z - 1;
        break;
      case 1:
        this._orientationMaxIndex = dimensionsIJK.x - 1;
        break;
      case 2:
        this._orientationMaxIndex = dimensionsIJK.y - 1;
        break;
      default:
        // do nothing!
        break;
    }
  }

  private _computeOrientationSpacing() {
    const spacing = this._stack.spacing;
    switch (this._sliceOrientation) {
      case 0:
        this._orientationSpacing = spacing.z;
        break;
      case 1:
        this._orientationSpacing = spacing.x;
        break;
      case 2:
        this._orientationSpacing = spacing.y;
        break;
      default:
        this._orientationSpacing = 0;
        break;
    }
  }

  /**
   * Given orientation, check if index is in/out of bounds.
   *
   * @private
   */
  private _isIndexOutOfBounds() {
    this._computeOrientationMaxIndex();
    if (this._sliceIndex >= this._orientationMaxIndex || this._sliceIndex < 0) {
      this._outOfBounds = true;
      window.console.error('Slice index is out of bounds!');
    } else {
      this._outOfBounds = false;
    }
  }

  /**
   * Release the stack helper memory including the slice memory.
   *
   * @public
   */
  public dispose() {
    this.remove(this._slice);
    this._slice.dispose();
    this._slice = null;
    this._border.dispose();
    this._border = null;
  }
}
