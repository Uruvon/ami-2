import Constants from '../Constants';
import { VolumeMaterial } from '../materials/VolumeMaterial';
import { StackModel } from '../models/StackModel';
import { BaseRenderer } from './BaseRenderer';

export class VolumeRenderer extends BaseRenderer {
  private _alphaCorrection: number = 0.5;
  public get alphaCorrection(): number {
    return this._alphaCorrection;
  }
  public set alphaCorrection(value: number) {
    this._alphaCorrection = value;
    this._material.uniforms.uAlphaCorrection.value = this._alphaCorrection;
  }

  private _shininess: number = 10.0;
  public get shininess(): number {
    return this._shininess;
  }
  public set shininess(value: number) {
    this._shininess = value;
    this._material.uniforms.uShininess.value = this._shininess;
  }

  private _steps: number = 32;
  public get steps(): number {
    return this._steps;
  }
  public set steps(value: number) {
    if (
      value < Constants.VOLUME.maxSteps &&
      value > Constants.VOLUME.minSteps
    ) {
      this._steps = value;
      this._material.uniforms.uSteps.value = this._steps;
    } else {
      window.console.warn(
        'WARNING: Tried to set steps outside of valid range, please set steps between: ',
        Constants.VOLUME.minSteps,
        ' and ',
        Constants.VOLUME.maxSteps,
        '.'
      );
    }
  }

  private _offset: number = 0;
  public get offset(): number {
    return this._offset;
  }
  public set offset(value: number) {
    this._offset = value;
  }

  private _textureLUT: THREE.Texture;
  public get textureLUT(): THREE.Texture {
    return this._textureLUT;
  }
  public set textureLUT(value: THREE.Texture) {
    this._textureLUT = value;
    this._material.uniforms.uTextureLUT.value = this._textureLUT;
  }

  set windowCenter(value: number) {
    this._windowCenter = value;
    this._material.uniforms.uWindowCenterWidth.value = [
      this._windowCenter - this._offset,
      this._windowWidth
    ];
  }
  set windowWidth(value: number) {
    this._windowWidth = value;
    this._material.uniforms.uWindowCenterWidth.value = [
      this._windowCenter - this._offset,
      this._windowWidth
    ];
  }
  set interpolation(interpolation: number) {
    this._interpolation = interpolation;

    if (interpolation === 0) {
      this._material = VolumeMaterial.idnMaterial;
    } else {
      this._material = VolumeMaterial.triMaterial;
    }

    this._prepareMaterial();
  }

  constructor(stack: StackModel) {
    super(stack);
    this._init();
    this._create();
  }

  protected _init() {
    this._material = VolumeMaterial.triMaterial;
    this._prepareStack();
    this._prepareTexture();
    this._prepareMaterial();
    this._prepareGeometry();
  }

  protected _create() {
    this._material.needsUpdate = true;
    this._mesh = new THREE.Mesh(this._geometry, this._material);
    this.add(this._mesh);
  }

  private _prepareStack() {
    if (!this._stack.prepared) {
      this._stack.prepare();
    }

    if (!this._stack.packed) {
      this._stack.pack();
    }

    // compensate for the offset to only pass > 0 values to shaders
    // models > models.stack.js : _packTo8Bits
    this._offset = Math.min(0, this._stack.minMax[0]);
    this._windowCenter = this._stack.windowCenter;
    this._windowWidth = this._stack.windowWidth * 0.8; // multiply for better default visualization
  }

  private _prepareMaterial() {
    // uniforms
    this._material.uniforms.uWorldBBox.value = this._stack.worldBoundingBox();
    this._material.uniforms.uTextureSize.value = this._stack.textureSize;
    this._material.uniforms.uTextureContainer.value = this._textures;
    this._material.uniforms.uWorldToData.value = this._stack.lps2IJK;
    this._material.uniforms.uNumberOfChannels.value = this._stack.numberOfChannels;
    this._material.uniforms.uPixelType.value = this._stack.pixelType;
    this._material.uniforms.uBitsAllocated.value = this._stack.bitsAllocated;
    this._material.uniforms.uPackedPerPixel.value = this._stack.packedPerPixel;
    this._material.uniforms.uWindowCenterWidth.value = [
      this._windowCenter - this._offset,
      this._windowWidth
    ];
    this._material.uniforms.uRescaleSlopeIntercept.value = [
      this._stack.rescaleSlope,
      this._stack.rescaleIntercept
    ];
    this._material.uniforms.uDataDimensions.value = [
      this._stack.dimensionsIJK.x,
      this._stack.dimensionsIJK.y,
      this._stack.dimensionsIJK.z
    ];
    this._material.uniforms.uAlphaCorrection.value = this._alphaCorrection;
    this._material.uniforms.uShininess.value = this._shininess;

    this._material.needsUpdate = true;
  }

  private _prepareGeometry() {
    const worldBBox = this._stack.worldBoundingBox();
    const centerLPS = this._stack.worldCenter();

    this._geometry = new THREE.BoxGeometry(
      worldBBox[1] - worldBBox[0],
      worldBBox[3] - worldBBox[2],
      worldBBox[5] - worldBBox[4]
    );
    this._geometry.applyMatrix(
      new THREE.Matrix4().makeTranslation(centerLPS.x, centerLPS.y, centerLPS.z)
    );
  }

  public dispose() {
    for (let j = 0; j < this._textures.length; j++) {
      this._textures[j].dispose();
      this._textures[j] = null;
    }
    this._textures = null;

    // material, geometry and mesh
    this.remove(this._mesh);
    this._mesh.geometry.dispose();
    this._mesh.geometry = null;
    (this._mesh.material as THREE.ShaderMaterial).dispose();
    this._mesh.material = null;
    this._mesh = null;

    this._geometry.dispose();
    this._geometry = null;
    this._material.vertexShader = null;
    this._material.fragmentShader = null;
    this._material.uniforms = null;
    this._material.dispose();
    this._material = null;

    this._stack = null;
  }
}
