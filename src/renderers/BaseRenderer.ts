import { StackModel } from '../models/StackModel';

export abstract class BaseRenderer extends THREE.Object3D {
  protected _stack: StackModel;
  get stack() {
    return this._stack;
  }
  set stack(value: StackModel) {
    this._stack = value;
  }

  // default to trilinear interpolation
  protected _interpolation: number;
  get interpolation() {
    return this._interpolation;
  }

  protected _windowWidth: number;
  get windowWidth(): number {
    return this._windowWidth;
  }
  set windowWidth(value: number) {
    this._windowWidth = value;
  }

  protected _windowCenter: number;
  get windowCenter(): number {
    return this._windowCenter;
  }
  set windowCenter(value: number) {
    this._windowCenter = value;
  }

  protected _material: THREE.ShaderMaterial;

  protected _textures: THREE.DataTexture[];

  protected _geometry: any;
  get geometry() {
    return this._geometry;
  }
  set geometry(geometry: any) {
    this._geometry = geometry;
  }

  protected _mesh: THREE.Mesh;
  get mesh() {
    return this._mesh;
  }
  set mesh(mesh: THREE.Mesh) {
    this._mesh = mesh;
  }

  constructor(stack: StackModel) {
    super();

    this._windowWidth = 0.0;
    this._interpolation = 1;
    this._windowCenter = 1.0;
    this._textures = [];

    this._stack = stack;
  }

  public hasUniforms() {
    return this._material.uniforms;
  }

  protected _prepareTexture() {
    this._textures = [];

    for (const rawDatum of this._stack.rawData) {
      if (rawDatum) {
        const tex = new THREE.DataTexture(
          rawDatum,
          this._stack.textureSize,
          this._stack.textureSize,
          this._stack.textureType,
          THREE.UnsignedByteType,
          THREE.UVMapping,
          THREE.ClampToEdgeWrapping,
          THREE.ClampToEdgeWrapping,
          THREE.NearestFilter,
          THREE.NearestFilter
        );
        tex.needsUpdate = true;
        tex.flipY = true;
        this._textures.push(tex);
      }
    }
  }

  protected abstract _init();
  protected abstract _create();
}
